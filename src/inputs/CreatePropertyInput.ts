import {InputType, Field} from 'type-graphql'
import {Column} from 'typeorm'

@InputType()
export class CreatePropertyInput {

    @Column()
    @Field()
    street: string

    @Column()
    @Field()
    city: string

    @Column()
    @Field()
    state: string

    @Column()
    @Field()
    zip: string

    @Column()
    @Field()
    rent: number
}
