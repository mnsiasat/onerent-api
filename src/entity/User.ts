import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany} from 'typeorm'
import {Field, ObjectType} from 'type-graphql'
import {Property} from './Property'

@Entity({name: 'users'})
@ObjectType()
export class User extends BaseEntity{

    @PrimaryGeneratedColumn('uuid')
    @Field()
    id: string;

    @Column({
        name: 'first_name',
    })
    @Field()
    firstName: string;

    @Column({
        name: 'last_name',
    })
    @Field()
    lastName: string;

    @OneToMany(type => Property, property => property.user, { cascade: true, eager:true })
    @Field(type => [Property])
    properties: Property[];

}
