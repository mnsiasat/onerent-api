module.exports = {
    'type': 'postgres',
    'host': 'localhost',
    'port': 5432,
    'username': 'onerent_admin',
    'password': 'pass123',
    'database': 'onerent_db',
    'synchronize': true,
    'logging': true,
    'autoLoadEntities': true,
    'entities': [
        'src/entity/*.ts'
    ],
    'migrations': [
        'src/migration/*.ts'
    ],
    'subscribers': [
        'src/subscriber/*.ts'
    ],
    'cli': {
        'entitiesDir': 'src/entity',
        'migrationsDir': 'src/migration',
        'subscribersDir': 'src/subscriber'
    }
};
//# sourceMappingURL=ormconfig.js.map