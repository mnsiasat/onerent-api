import {InputType, Field} from 'type-graphql'
import {Column} from 'typeorm'
import {CreatePropertyInput} from './CreatePropertyInput'

@InputType()
export class CreateUserInput {
  @Field()
  firstName: string

  @Field()
  lastName: string

  @Field(type => [CreatePropertyInput])
  properties: CreatePropertyInput[]
}
