import {Resolver, Query, Mutation, Arg} from 'type-graphql'
import {User} from '../entity/User'
import {CreateUserInput} from '../inputs/CreateUserInput'
import {Property} from '../entity/Property'

@Resolver(of => User)
export class UserResolver {
    @Query(() => [User])
    async users() {
        return await User.find()
    }

    @Query(() => User)
    async user(@Arg('id') id: string) {
        return await User.findOne({where: {id}})
    }

    @Mutation(() => User)
    async createUser(@Arg('data') data: CreateUserInput) {
        const user:User = User.create(data)
        await user.save()
        return user
    }

}
