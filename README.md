# OneRent API Build with TypeORM and TypeGraphQL

Steps to run this project:

1. Run `npm i` command
2. Setup your local database 
3. Create `.env` file inside your root directory containing details for `DB, DB_HOST, DB_USER, DB_PWD`
4. Run `npm run start:dev` command
5. You may import my postman collection to add data to your local DB [ONE RENT.postman_collection.json](https://gitlab.com/mnsiasat/onerent-api/-/blob/c244eafa9630fa2b477890dbfb588aea54278032/ONE%20RENT.postman_collection.json)
