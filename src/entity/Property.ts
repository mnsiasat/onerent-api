import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne} from 'typeorm'
import {Field, ObjectType} from 'type-graphql'
import {User} from './User'

@Entity({name: 'properties'})
@ObjectType()
export class Property extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    @Field()
    id: string

    @Column()
    @Field()
    street: string

    @Column()
    @Field()
    city: string

    @Column()
    @Field()
    state: string

    @Column()
    @Field()
    zip: string

    @Column()
    @Field()
    rent: number

    @ManyToOne(type => User, user => user.properties)
    @Field(type=>User)
    user: User;
}
