import {User} from './User'
import {Property} from './Property'
import {createUnionType} from 'type-graphql'

// @ts-ignore
export const SearchResult = createUnionType({
    name: 'SearchResult',
    types: () => [User, Property],
    resolveType: value => {
        if ("lastName" in value) {
            return User;
        }
        if ("street" in value) {
            return Property;
        }
        return undefined;
    },
})
