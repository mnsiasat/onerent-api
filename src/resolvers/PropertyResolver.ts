import {Resolver, Query, Mutation, Arg} from 'type-graphql'
import {Property} from '../entity/Property'
import {CreatePropertyInput} from '../inputs/CreatePropertyInput'

@Resolver(of => Property)
export class PropertyResolver {
    @Query(() => [Property])
    properties() {
        return Property.find()
    }

    @Query(() => Property)
    property(@Arg('id') id: string) {
        return Property.findOne({where: {id}})
    }

    @Mutation(() => Property)
    async createProperty(@Arg('data') data: CreatePropertyInput) {
        const property = Property.create(data)
        await property.save()
        return property
    }

}
