import {Resolver, Query, Mutation, Arg} from 'type-graphql'
import {User} from '../entity/User'
import {CreateUserInput} from '../inputs/CreateUserInput'
import {Property} from '../entity/Property'
import {SearchResult} from '../entity/SearchResult'
import {Raw} from 'typeorm'


@Resolver()
export class SearchEngineResolver {
    @Query(returns => [SearchResult])
    async search(@Arg("text") text: string): Promise<Array<typeof SearchResult>> {
        const users = await User.find({
            where: [{
                firstName: Raw(alias => `${alias} ILIKE '%${text}%'`),
            }, {
                lastName: Raw(alias => `${alias} ILIKE '%${text}%'`)
            }]
        });
        const properties = await Property.find({
            where: [{
                street: Raw(alias => `${alias} ILIKE '%${text}%'`)
            },{
                city: Raw(alias => `${alias} ILIKE '%${text}%'`)
            },{
                state: Raw(alias => `${alias} ILIKE '%${text}%'`)
            },{
                zip: Raw(alias => `${alias} ILIKE '%${text}%'`)
            }]
        });
        return [...users, ...properties];
    }
}
